To use this project, it is best if you have a program like Visual Studio Code or Brackets to view, edit and run the code. 
If you do not have one of these, Notepad works just fine.
Download the html files and view in your file explorer. Open the file in your preferred code editor and code away! 

I added the mit license to this project because it is short and gets to the point. Most people are lazy and skim through licenses quickly 
so I think it is important to use something short so that people will actually read and understand it.